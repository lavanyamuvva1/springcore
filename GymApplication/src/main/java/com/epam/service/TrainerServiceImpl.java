package com.epam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dto.request.TrainerDto;
import com.epam.dto.request.TrainerTrainingsList;
import com.epam.dto.request.TrainerUpdateDto;
import com.epam.dto.response.CredentialsDto;
import com.epam.dto.response.NotificationDto;
import com.epam.dto.response.TrainerProfileDto;
import com.epam.dto.response.TrainingDetailsDto;
import com.epam.entity.Trainer;
import com.epam.entity.Training;
import com.epam.entity.TrainingType;
import com.epam.entity.User;
import com.epam.exception.UserException;
import com.epam.kafka.Producer;
import com.epam.repository.TrainerRepository;
import com.epam.repository.TrainingTypeRepository;
import com.epam.repository.UserRepository;
import com.epam.utils.ServiceMapper;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TrainerServiceImpl implements TrainerService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	TrainerRepository trainerRepository;

	@Autowired
	TrainingTypeRepository trainingTypeRepository;

	@Autowired
	ServiceMapper serviceMapper;
	
	@Autowired
	Producer producer;
	
	static final String TRAINER_EXCEPTION = "Trainer with username not found";

	@Override
	public CredentialsDto addTrainer(TrainerDto trainerDto) {
		log.info("Entered into Create Trainer Method :{}",trainerDto);
		User user = serviceMapper.createUserTrainerProfile(trainerDto);
		userRepository.save(user);
		TrainingType trainingType=trainingTypeRepository.findByTrainingTypeName(trainerDto.getSpecialization()).orElseGet(()->{
			return trainingTypeRepository.save(TrainingType.builder().trainingTypeName(trainerDto.getSpecialization()).build());
		});
		Trainer trainer = Trainer.builder().trainingType(trainingType).user(user).build();
		trainerRepository.save(trainer);
		CredentialsDto credentialsDto=CredentialsDto.builder().username(user.getUsername()).password(user.getPassword()).build();
		NotificationDto dto=serviceMapper.getRegistrationNotification(user);
		producer.sendNotificationLog(dto);
		return credentialsDto;
	}

	@Override
	public TrainerProfileDto getTrainerProfile(String username) {
		log.info("Entered into get Trainer Profile of {}",username);
		return trainerRepository.findByUserUsername(username).map(trainer->{
			return serviceMapper.getTrainerProfile(trainer);
		}).orElseThrow(() -> new UserException(TRAINER_EXCEPTION));
		
	}

	@Override
	@Transactional
	public TrainerProfileDto updateTrainerProfile(TrainerUpdateDto updateDto) {
		log.info("Entered into update Trainer Profile of {}",updateDto);
		return trainerRepository.findByUserUsername(updateDto.getUsername()).map(trainer->{
			User user = trainer.getUser();
			user.setActive(true);
			user.setEmail(updateDto.getEmail());
			user.setFirstName(updateDto.getFirstName());
			user.setLastName(updateDto.getLastName());
			updateDto.setSpecialization(trainer.getTrainingType().getTrainingTypeName());
			trainer.setUser(user);
			NotificationDto dto=serviceMapper.getTrainerUpdateNotification(updateDto);
			producer.sendNotificationLog(dto);
			log.info("Retriving Updated trainee Profile");
			return serviceMapper.getTrainerProfile(trainer);
		}).orElseThrow(() -> new UserException(TRAINER_EXCEPTION));
	}

	@Override
	public List<TrainingDetailsDto> getTrainerTrainingsList(TrainerTrainingsList trainerTrainingsList) {
		log.info("Entered into getTrainerTrainingsList");
		return trainerRepository.findByUserUsername(trainerTrainingsList.getUsername()).map(trainer->{
			List<Training> trainingsList = trainerRepository.findTrainingsForTrainer(trainerTrainingsList.getUsername(),
					trainerTrainingsList.getPeriodFrom(), trainerTrainingsList.getPeriodTo(),
					trainerTrainingsList.getTraineeName());
			log.info("Retriving training details of trainee");
			return serviceMapper.getTrainingDetailsList(trainingsList);
		}).orElseThrow(()->new UserException(TRAINER_EXCEPTION));
		
		

	}

}
